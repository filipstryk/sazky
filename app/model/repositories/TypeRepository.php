<?php

namespace App\Model\Repositories;

use Nette\Object;
use Kdyby\Doctrine\EntityManager;
use App\Model\Entities\TypeEntity;

/**
 * Třída TypeRepository
 * @package App\Model\Repository
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class TypeRepository extends Object
{
	/**
	 * @var EntityManager
	 */
	private $em;

	private $repository;

	/**
	 * Konstruktor třídy TypeRepository
	 * @param EntityManager $entityManager
	 */
	public function __construct(EntityManager $entityManager) {
		$this->em = $entityManager;
		$this->repository = $this->em->getRepository(TypeEntity::getClassName());
	}

	/**
	 * Vrátí druh podle ID
	 * @param int $id ID typu
	 * @return mixed|null|object
	 */
	public function findById($id) {
		return $this->repository->findOneBy(['id' => $id]);
	}

	/**
	 * Vrátí druh podle názvu
	 * @param string $name Název typu
	 * @return mixed|null|object
	 */
	public function findByName($name) {
		return $this->repository->findOneBy(['name' => $name]);
	}

	/**
	 * Vrátí všechny druhy
	 * @param array $orderBy Určuje seřazení typů
	 */
	public function findAll($orderBy = null) {
		return $this->repository->findBy([], $orderBy);
	}

	/**
	 * @return array
	 */
	public function getPairs() {
		return $this->repository->findPairs('name');
	}


}