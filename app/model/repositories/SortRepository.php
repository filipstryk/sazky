<?php

namespace App\Model\Repositories;

use Nette\Object;
use Kdyby\Doctrine\EntityManager;
use App\Model\Entities\SortEntity;

/**
 * Třída SortsRepository
 * @package App\Model\Repositories
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class SortRepository extends Object
{
	/**
	 * @var EntityManager $em
	 */
	private $em;

	private $repository;

	/**
	 * Konstruktor třídy SortsRepository
	 * @param EntityManager $entityManager
	 */
	public function __construct(EntityManager $entityManager) {
		$this->em = $entityManager;
		$this->repository = $entityManager->getRepository(SortEntity::getClassName());
	}

	/**
	 * Vrátí druh podle ID
	 * @param int $id ID druhu
	 * @return mixed|null|object
	 */
	public function findById($id) {
		return $this->repository->findOneBy(['id' => $id]);
	}

	/**
	 * Vrátí druh podle názvu
	 * @param string $name Název druhu
	 * @return mixed|null|object
	 */
	public function findByName($name) {
		return $this->repository->findOneBy(['name' => $name]);
	}

	/**
	 * Vrátí všechny druhy
	 * @param array $orderBy Určuje seřazení druhů
	 */
	public function findAll($orderBy = null) {
		return $this->repository->findBy([], $orderBy);
	}

	/**
	 * @return array
	 */
	public function getPairs() {
		return $this->repository->findPairs('name');
	}
}