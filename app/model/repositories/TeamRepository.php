<?php

namespace App\Model\Repositories;

use Nette\Object;
use Kdyby\Doctrine\EntityManager;
use App\Model\Entities\TeamEntity;

/**
 * Třída TeamRepository
 * @package App\Model\Repository
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class TeamRepository extends Object
{
	/**
	 * @var EntityManager
	 */
	private $em;

	private $repository;

	/**
	 * Konstruktor třídy TeamRepository
	 * @param EntityManager $entityManager
	 */
	public function __construct(EntityManager $entityManager) {
		$this->em = $entityManager;
		$this->repository = $this->em->getRepository(TeamEntity::getClassName());
	}

	/**
	 * Vrátí tým podle ID
	 * @param int $id ID tým
	 * @return mixed|null|object
	 */
	public function findById($id) {
		return $this->repository->findOneBy(['id' => $id]);
	}

	/**
	 * Vrátí tým podle názvu
	 * @param string $name Název týmu
	 * @return mixed|null|object
	 */
	public function findByName($name) {
		return $this->repository->findOneBy(['name' => $name]);
	}

	/**
	 * Vrátí všechny týmy
	 * @param array $orderBy Určuje seřazení týmů
	 */
	public function findAll($orderBy = null) {
		return $this->repository->findBy([], $orderBy);
	}
}