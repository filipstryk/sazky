<?php

namespace App\Model\Repositories;

use Kdyby;
use Nette;
use App\Model\Entities;

/**
 * Třída UserRepository
 * @package App\Model\Repositories;
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class UserRepository extends Nette\Object
{
	/**
	 * @var Kdyby\Doctrine\EntityManager
	 */
	private $em;

	private $repository;

	/**
	 * Konstruktor třídy UserRepository
	 * @param Kdyby\Doctrine\EntityManager $entityManager
	 */
	public function __construct(Kdyby\Doctrine\EntityManager $entityManager) {
		$this->em = $entityManager;
		$this->repository = $entityManager->getRepository(Entities\UserEntity::getClassName());
	}

	/**
	 * @param int $id ID uživatele
	 * @return mixed|null|object
	 */
	public function findById($id) {
		return $this->repository->findOneBy(['id' => $id]);
	}

	/**
	 * @param string $username Jméno uživatele
	 * @return mixed|null|object
	 */
	public function findByUsername($username) {
		return $this->repository->findOneBy(['username' => $username]);
	}

	/**
	 * @param array Pravidle, pro vyhledání uživatelů
	 * @return mixed|null|object
	 */
	public function findBy($criteria) {
		return $this->repository->findBy($criteria);
	}

	/**
	 * @return mixed|null|object
	 */
	public function findAll($orderBy = null) {
		return $this->repository->findBy([], $orderBy);
	}

	/**
	 * @return array
	 */
	public function getPairs() {
		return $this->repository->findPairs('username');
	}
}