<?php

namespace App\Listeners;

use Nette;
use Nette\Security\User;
use Kdyby\Events\Subscriber;
use App\Model\Entities\LoginEntity;
use Kdyby\Doctrine\EntityManager;

class UserLoggingLoginListener extends Nette\Object implements Subscriber
{

	/**
	 * @var EntityManager $em
	 */
	private $em;

	public function __construct(EntityManager $entityManager) {
		//$his->em = $entityManager;
	}

	public function getSubscribedEvents() {
		return [
			'Nette\Security\User::onLoggedIn',
			'Nette\Security\User::onLoggedOut'
		];
	}

	public function onLoggedIn(User $user) {
		/*$user = new LoginEntity($user->id);
		$this->em->persist($user);
		$this->em->flush();*/
	}

	public function onLoggedOut(User $user) {
		
	}
}