<?php

namespace App\Model\Entities;

use Nette\Utils\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * Třída LoginEntity
 * @package App\Model\Entities;
 * @author Filip Stryk <filipstryk@hotmail.com>
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="logging")
 */
class LoginEntity extends BaseEntity
{
	use MagicAccessors;
	use Identifier;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Model\Entities\UserEntity")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 * @var UserEntity Uživatel, kerý se přihlásil
	 */
	private $user;

	/**
	 * @ORM\Column(type="datetime", nullable=false)
	 * @var DateTime Čas a datum přihlášení
	 */
	private $date;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string Prohlížeč, ze kterého se uživatel přihlásil
	 */
	private $browser;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string IP, ze které se uživatel přihlásil
	 */
	private $ip;

	public function __construct($userId) {
		$this->user = $userId;
		$this->date = new DateTime('now');
		$this->browser = $_SERVER['HTTP_USER_AGENT'];
		$this->ip = $_SERVER['REMOTE_ADDR'];
	}


}