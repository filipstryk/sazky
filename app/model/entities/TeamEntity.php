<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * Třída TeamEntity
 * @package App\Model\Entities
 * @author Filip Stryk <filipstyk@hotmail.com>
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="teams")
 */
class TeamEntity extends BaseEntity
{
	use MagicAccessors;
	use Identifier;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string $name Název týmu
	 */
	protected $name;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Model\Entities\TypeEntity")
	 * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
	 * @var TypeEntity $type Typ týmu
	 */
	protected $type;

	/**
	 * Konstruktor třídy TeamEntity
	 * @param string $name Název týmu
	 * @param TypeEntity $$type Typ týmu
	 */
	public function __construct($name, $type) {
		$this->name = $name;
		$this->type =$type;
	}
}