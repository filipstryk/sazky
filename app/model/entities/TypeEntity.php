<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * Třída TypeEntity
 * @package App\Model\Entities
 * @author Filip Stryk <filipstryk@hotmail.com>
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="types")
 */
class TypeEntity extends BaseEntity
{
	use MagicAccessors;
	use Identifier;

	/**
	 * @ORM\Column(type="string", nullable=false, unique=true)
	 * @var string $name Název typu
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string $color Barva typu
	 */
	private $color;

	/**
	 * @ORM\ManyToOne(targetEntity="SortEntity", inversedBy="types")
	 * @ORM\JoinColumn(name="sort_id", referencedColumnName="id")
	 * @var SortEntity Druh typu
	 */
	protected $sort;

	/**
	 * Konstruktor třídy TypeEnity
	 * @param string $name Název typu
	 * @param string $color Barva tyu
	 * @param SortEntity $sort Druh typu
	 */
	public function __construct($name, $color, $sort) {
		$this->name = $name;
		$this->color = $color;
		$this->sort = $sort;
	}

	/**
	 * Nastaví název druhu
	 * @param string $name Nový název druhu
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Vrátí název druhu
	 * @return string Název druhu
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Nastaví barvu druhu
	 * @param string $color Barva druhu
	 */
	public function setColor($color) {
		$this->color = $color;
	}

	/**
	 * Vrátí barvu druhu
	 * @return stringBarva druhu
	 */
	public function getColor() {
		return $this->color;
	}
}