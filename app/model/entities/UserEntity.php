<?php

namespace App\Model\Entities;

use Nette\Utils\Random;
use Nette\Security\Passwords;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;
use Doctrine\Common\Collections\Collection;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * Třída UserEntity
 * @package App\Model\Entities
 * @author Filip Stryk <filipstryk@hotmail.com>
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class UserEntity extends BaseEntity
{
	use MagicAccessors;
	use Identifier;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string Jméno uřivatele
	 */
	private $username;

	/**
	 * @ORM\Column(type="string", nullable=false, unique=true)
	 * @var string Heslo uživatele
	 */
	private $password;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string Role uživatele
	 */
	private $role;

	/**
	 * @var array Seznam uživatelský rolí
	 */
	static private $roleList = [
		'admin' => 'admin',
		'user' => 'user'];

	/**
	 * @ORM\Column(type="bigint", options={"default":0})
	 * @var bigint Konto uživatele
	 */
	private $account;

	/**
	 * @ORM\Column(type="string", nullable=false, options={"default":"active"})
	 * @var string Stav uživatele
	 */
	private $state;

	/**
	 * @var array Seznam možných stavů uživatele
	 */
	static private $statesList = [
		'active' => 'active',
		'inactive' => 'inactive'];


	/**
	 * Konstruktor třídy UserEntity
	 * @param string $username Uživatelské jméno
	 * @param string $passwird Heslo uživatele
	 * @param string $role Role uživatele
	 */
	public function __construct($username, $password, $role = 'user') {
		$this->username = $username;
		$this->account = 0;
		$this->setState('active');
		$this->setPassword($password);
		$this->setRole($role);
	}

	/**
	 * Nastaví uživateli jméno
	 * @param string $username Jméno uživatele
	 */
	public function setUsername($username) {
		$this->username = $username;
	}

	/**
	 * Vrátí jméno uživatele
	 * @return string Jméno uživatele
	 */
	public function getUsername() {
		return $this->username;
	}

	/**
	 * Nastaví uživateli heslo
	 * @param string $password Heslo uživatele
	 */
	public function setPassword($password) {
		$this->password = Passwords::hash($password);
	}

	/**
	 * Vrátí hash hesla uživatele
	 * @return string Heslo uživatele
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * Nastaví uživateli roli
	 * @param string $role Role, která se má nastavit uživateli
	 */
	public function setRole($role) {
		if (in_array($role, self::$roleList))
			$this->role = $role;
	}

	/**
	 * Vratí roli uživatele
	 * @return string Role uživatele
	 */
	public function getRole() {
		return $this->role;
	}

	/**
	 * Vrátí pole uživatelských rolí
	 * @return array Pole uživatelských rolí
	 */
	public static function getRoleList() {
		return self::$roleList;
	}

	/**
	 * Přičte/Odečte částku z konta uživatele
	 * @param string $action Udává, zda se má částka přičíst, nebo odečíst
	 * @param int $amount Částka, která se má přičíst/odečíst
	 */
	public function setAccount($action = '+', $amount = 0) {
		if ($action === '+')
			$this->account += $amount;
		else
			$this->account -= $amount;
	}

	/**
	 * Vrátí stav uživatelského konta
	 * @return int Stav uživatelského konta
	 */
	public function getAccount() {
		return $this->account;
	}

	/**
	 * Aktivuje/Deaktivuje uživatelský účet
	 * @param string $state Stav, terý se má uživateli nastavit
	 */
	public function setState($state = 'inactive') {
		if (in_array($state, self::$statesList))
			$this->state = $state;
	}

	/**
	 * Vrátí stav uživatele
	 * @return string Stav uživatele
	 */
	public function getState() {
		return $this->state;
	}

	/**
	 * Vrátí TRUE, pokud je uživatel aktivní a FALSE, pokud je neaktivní
	 * @return bool Stav uživatele
	 */
	public function isActive() {
		if ($this->state === 'active')
			return TRUE;
		else
			return FALSE;
	}

	/**
	 * Obnoví uživateli zapomenuté heslo
	 * @return string Nové heslo
	 */
	public function recoverPassword() {
		$newPassword = Random::generate(15, '0-9a-zA-Z');
		$this->setPassword($newPassword);

		return $newPassword;
	}

	/**
	 * Aktivuje/Deaktivuje uživatele
	 * @return int Provedená akce(0 - deaktivace, 1 - aktivace)
	 */
	public function changeState() {
		if ($this->state === 'active') {
			$this->setState('inactive');
			return 0;
		}
		else {
			$this->setState('active');
			return 1;
		}
	}

}