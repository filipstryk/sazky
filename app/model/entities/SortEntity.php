<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Třída SortEntity
 * @package App\Model\Entities
 * @author Filip Stryk <filipstryk@hotmail.com>
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="sorts")
 */
class SortEntity extends BaseEntity
{
	use MagicAccessors;
	use Identifier;

	/**
	 * @ORM\Column(type="string", nullable=false, unique=true)
	 * @var string Název druhu
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string Barva druhu
	 */
	private $color;

	/**
	 * @ORM\OneToMany(targetEntity="TypeEntity", mappedBy="sort")
	 * @var ArrayCollection
	 */
	private $types;

	/**
	 * Konstruktor třídy SortEntity
	 * @param string $name Název druhu
	 * @param string $color Barva druhu
	 */
	public function __construct($name, $color) {
		$this->name = $name;
		$this->color = $color;
		$this->types = new ArrayCollection();
	}

	/**
	 * Nastaví název druhu
	 * @param string $name Nový název druhu
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Vrátí název druhu
	 * @return string Název druhu
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Nastaví barvu druhu
	 * @param string $color Barva druhu
	 */
	public function setColor($color) {
		$this->color = $color;
	}

	/**
	 * Vrátí barvu druhu
	 * @return stringBarva druhu
	 */
	public function getColor() {
		return $this->color;
	}
}