<?php

namespace App\Model\Entities;

use Nette\Utils\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * Třída MatchEntity
 * @package App\Model\Entities
 * @author Filip Stryk <filipstryk@hotmail.com>
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="matches")
 */
class MatchEntity
{
	use MagicAccessors;
	use Identifier;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Model\Entities\TeamEntity")
	 * @ORM\JoinColumn(name="tema_a", referencedColumnName="id")
	 * @var TeamEntity Tým A
	 */
	protected $teamA;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Model\Entities\TeamEntity")
	 * @ORM\JoinColumn(name="team_b", referencedColumnName="id")
	 * @var TeamEntity Tým B
	 */
	protected $teamB;

	/**
	 * @ORM\Column(type="datetime", nullable=false)
	 * @var DateTime Čas odehrání zápasu
	 */
	protected $date;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Model\Entities\TypeEntity")
	 * @ORM\JoinColumn(name="type", referencedColumnName="id")
	 * @var TypeEntity Typ zápasu
	 */
	protected $type;

	/**
	 * @ORM\Column(name="course_a", type="float", options={"default":0.0})
	 * @var float Kurz na tým A
	 */
	protected $courseA;

	/**
	 * @ORM\Column(name="course_draw", type="float", options={"default":0.0})
	 * @var float Kurz na remízu
	 */
	protected $courseDraw;

	/**
	 * @ORM\Column(name="course_b", type="float", options={"default":0.0})
	 * @var float Kurz na tým B
	 */
	protected $courseB;

	/**
	 * @ORM\Column(type="smallint", nullable=true, options={"default":0})
	 * @var smallint Stav zápasu[0 - neodehrán; 1 - hrající se; 2 - ukončen, bez výsledku; 3 - ukončen, s výsledky; 4 - vyhodnocen]
	 */
	protected $state;

	public function __construct(TeamEntity $teamA, TeamEntity $teamB, DateTime $date, 
		TypeEntity $type, float $courseA, float $courseDraw, float $courseB, $state) {
		
	}
}