<?php

namespace App\Model;

use Nette;
use Kdyby;
use Nette\Security as NS;

/**
 * Třída Authenticator
 * @package App\Model
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class Authenticator extends Nette\Object implements NS\IAuthenticator
{
	/**
	 * @var Repositories\UserRepository $userRepository
	 */
	private $userRepository;

	/**
	 *  @param Repositories\UserRepository $userRepository
	 */
	public function __construct(Repositories\UserRepository $userRepository) {
		$this->userRepository = $userRepository;
	}

	/**
	 * @param array $credentials
	 * @return NS\Identity
	 * @throws NS\AuthenticationException
	 */
	public function authenticate(array $credentials) {
		list($username, $password) = $credentials;

		/**
		 * @var Entities\UserEntity $user
		 */
		$user = $this->userRepository->findByUsername($username);

		if (!$user)
			throw new NS\AuthenticationException("Uživatel '$username' nenalezen.", self::IDENTITY_NOT_FOUND);

		if (!NS\Passwords::verify($password, $user->getPassword()))
			throw new NS\AuthenticationException("Neplatné heslo.", self::INVALID_CREDENTIAL);

		return new NS\Identity($user->getId(), $user->getRole(), [
			'username' => $user->getUsername(),
			'account' => $user->getAccount()
		]);
	}
}