<?php

namespace App\Presenters;

class BasePresenter extends \Nette\Application\UI\Presenter
{
	/** @var \WebLoader\Nette\LoaderFactory @inject */
	public $webLoader;

	/**
	 * @return CssLoader
	 */
	public function createComponentCss() {
		return $this->webLoader->createCssLoader('default');
	}

	/**
	 * @return JavaScriptLoader
	 */
	public function createComponentJs() {
		return $this->webLoader->createJavaScriptLoader('default');
	}
}