<?php

namespace App\FrontModule\Presenters;

class HomepagePresenter extends BaseFrontPresenter
{	
	/** @var \App\FrontModule\Components\IChangePasswordFormFactory @inject */
	public $changePasswordFormFactory;

	public function createComponentChangePasswordForm() {
		$form = $this->changePasswordFormFactory->create();

		return $form;
	}
}