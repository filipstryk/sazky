<?php

namespace App\FrontModule\Presenters;

use App\Presenters\BasePresenter;

class LogInPresenter extends BasePresenter
{	
	/** @var \App\FrontModule\Components\ILogInFormFactory @inject */
	public $logInFormFactory;

	public function beforeRender()  {
		if ($this->user->isLoggedIn())
			$this->redirect('Homepage:');
	}
	
	public function createComponentLogInForm() {
		$form = $this->logInFormFactory->create();

		return $form;
	}

	public function actionOut() {
		$this->user->logout();
		$this->flashMessage('Odhlášení proběhlo úspěšně!.', 'success');
		$this->redirect('LogIn:in');
	}
}