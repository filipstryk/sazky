<?php

namespace App\FrontModule\Presenters;

use App\Presenters\BasePresenter;

class BaseFrontPresenter extends BasePresenter
{
	public function startup() {
		parent::startup();
		if (!$this->user->isLoggedIn()) {
			$this->flashMessage('Pro přístup se musíte přihlásit!', 'danger');
			$this->redirect(':Front:LogIn:in');
		}
	}
}