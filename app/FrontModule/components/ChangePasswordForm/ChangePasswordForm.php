<?php

namespace App\FrontModule\Components;

use Nette\Security as NS;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use App\Model\Entities\UserEntity;
use App\Model\Repositories\UserRepository;
use Kdyby\Doctrine\EntityManager;

/**
 * Třída ChangePasswordForm
 * @package App\FrotnModule\Components
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class ChangePasswordForm extends Control
{
	/**
	 * @var NS\User $user
	 */
	private $user;

	/**
	 * @var UserRepository $userRepository
	 */
	private $userRepository;

	/**
	 * @var EntityManaer $em
	 */
	private $em;

	/**
	 * @param NS\User $user
	 */
	public function __construct(NS\User $user, UserRepository $userRepository, EntityManager $entityManager) {
		parent::__construct();
		$this->user = $user;
		$this->userRepository = $userRepository;
		$this->em = $entityManager;
	}

	/**
	 * @return Form
	 */
	public function createComponentChangePasswordForm() {
		$form = new Form();

		$form->addPassword('old_password', 'Současné heslo:')
			->addRule(Form::FILLED, 'Musíte zadat současné heslo!');

		$form->addPassword('new_password', 'Nové heslo:')
			->addRule(Form::FILLED, 'Musíte zadat nové heslo!');

		$form->addPassword('new_password_repeat', 'Nové heslo:')
			->addRule(Form::FILLED, 'Musíte zadat znova nové heslo pro kontrolu!');

		$form->addSubmit('send', 'Změnit');

		$form->onSuccess[] = $this->changePasswordFormSuccess;

		return $form;
	}

	/**
	 * @param Form $form
	 * @param array $values
	 */
	public function changePasswordFormSuccess(Form $form, $values) {
		if (!$this->user->isLoggedIn()) {
			$this->presenter->flashMessage('Nedostatečná oprávnění.', 'danger');
			$this->presenter->redirect(':LogIn:out');
		}

		$user = $this->userRepository->findById($this->user->id);

		if (!NS\Passwords::verify($values->old_password, $user->getPassword()) || $values->new_password !== $values->new_password_repeat) {
			$this->presenter->flashMessage('Hesla nesouhlasí!', 'danger');
			$this->presenter->redirect('this');
		}

		$user->setPassword($values->new_password);
		$this->em->flush();
		$this->presenter->flashMessage('Heslo bylo úspěšně změněno!', 'success');
		$this->presenter->redirect('this');

	}

	public function render() {
		$this->template->setFile(__DIR__ . '/changePasswordForm.latte');
		$this->template->render();
	}

}

interface IChangePasswordFormFactory {
	/** @return ChangePasswordForm */
	function create();
}