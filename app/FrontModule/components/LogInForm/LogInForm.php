<?php

namespace App\FrontModule\Components;

use Nette\Application\UI\Form;
use Nette\Application\UI\Control;

/**
 * Třída LogInForm
 * @package App\FrontModule\Components
 * @author Filip Stryk <filipstryl@hotmail.com>
 */
class LogInForm extends Control
{
	public function __construct() {
		parent::__construct();
	}

	/**
	 * @return Form
	 */
	public function createComponentLogInForm() {
		$form = new Form();

		$form->addText('username', 'Uživatelské jméno:')
			->addRule(Form::FILLED, 'Musíte zadat jméno!');

		$form->addPassword('password', 'Heslo:')
			->addRule(Form::FILLED, 'Musíte zadat heslo!');

		$form->addCheckbox('remember', 'Neodhlašovat');

		$form->addSubmit('send', 'Přihlásit');

		$form->onSuccess[] = $this->logInFormSuccess;

		return $form;
	}

	/**
	 * @param Form $form
	 * @param array $values
	 */
	public function logInFormSuccess(Form $form, $values) {
		try {
			$this->presenter->user->login($values->username, $values->password);
			if ($values->remember)
				$this->presenter->user->setExpiration('14 days', FALSE);
			else
				$this->presenter->user->setExpiration('20 minutes', FALSE);
			$this->getPresenter()->redirect('Homepage:');
		} catch (NS\AuthenticationException $e) {
			$this->getPresenter()->flashMessage('Špatné uživatelské jméno nebo heslo!', 'danger');
		}
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/logInForm.latte');
		$this->template->render();
	}
}

interface ILogInFormFactory {
	/** @return LogInForm */
	function create();
}