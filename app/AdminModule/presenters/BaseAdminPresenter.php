<?php

namespace App\AdminModule\Presenters;

use App\Presenters\BasePresenter;

class BaseAdminPresenter extends BasePresenter
{
	public function startup() {
		parent::startup();
		if (!$this->user->isLoggedIn()) {
			$this->flashMessage('Pro přístup se musíte přihlásit!', 'danger');
			$this->redirect(':Front:LogIn:in');
		}

		if (!$this->user->isInRole('admin')) {
			$this->flashMessage('Pro přístup do této sekce nemáte dostatečná oprávnění.', 'danger');
			$this->redirect(':Front:Homepage:');
		}
	}
}