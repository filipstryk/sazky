<?php

namespace App\AdminModule\Presenters;

class DefiningPresenter extends BaseAdminPresenter
{
	/**
	 * @inject
	 * @var \App\AdminModule\Components\IAddSortFormFactory
	 */
	public $addSortFormFactory;

	/**
	 * @inject
	 * @var \App\AdminModule\Components\IAddTypeFormFactory
	 */
	public $addTypeFormFactory;

	/**
	 * @inject
	 * @var \App\AdminModule\Components\IAddTeamFormFactory
	 */
	public $addTeamFormFactory;

	/**
	 * @inject
	 * @var \App\Model\Repositories\SortRepository
	 */
	public $sortRepository;

	/**
	 * @inject
	 * @var \App\Model\Repositories\TypeRepository
	 */
	public $typeRepository;

	/**
	 * @inject
	 * @var \App\Model\Repositories\TeamRepository
	 */
	public $teamRepository;

	public function renderDefault() {
		$this->template->sorts = $this->sortRepository->findAll(['id' => 'ASC']);
		$this->template->types = $this->typeRepository->findAll(['id' => 'ASC']);
		$this->template->teams = $this->teamRepository->findAll(['id' => 'ASC']);
	}

	public function createComponentAddSortForm() {
		return $this->addSortFormFactory->create();
	}

	public function createComponentAddTypeForm() {
		return $this->addTypeFormFactory->create();
	}

	public function createComponentAddTeamForm() {
		return $this->addTeamFormFactory->create();
	}
}