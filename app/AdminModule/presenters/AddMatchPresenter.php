<?php

namespace App\AdminModule\Presenters;

class AddMatchPresenter extends BaseAdminPresenter
{
	/**
	 * @inject
	 * @var \App\AdminModule\Components\ISelectTypeFormFactory
	 */
	public $selectTypeFormFactory;

	/**
	 * @inject
	 * @var \App\AdminModule\Components\ISetMatchDetailsFormFactory
	 */
	public $setMatchDetailsFormFactory;

	public function createComponentSelectTypeForm() {
		return $this->selectTypeFormFactory->create();
	}

	public function createComponentSetMatchDetailsForm() {
		return $this->setMatchDetailsFormFactory->create();
	}
}