<?php

namespace App\AdminModule\Presenters;

class UsersPresenter extends BaseAdminPresenter
{
	/**
	 * @inject
	 * @var \App\Model\Repositories\UserRepository
	 */
	public $userRepository;

	/**
	 * @inject
	 * @var \Kdyby\Doctrine\EntityManager
	 */
	public $em;

	/**
	 * @inject
	 * @var \App\AdminModule\Components\IAddUserFormFactory
	 */
	public $addUserFormFactory;

	/**
	 * @inject
	 * @var \App\AdminModule\Components\IChangeRoleFormFactory
	 */
	public $changeRoleFormFactory;

	public function renderDefault() {
		$this->template->users = $this->userRepository->findAll(['role' => 'ASC', 'state' => 'ASC', 'username' => 'ASC']);
	}

	public function handleRecoverPassword($userId) {
		$user = $this->userRepository->findById($userId);
		if (!$user) {
			$this->flashMessage('Neplatné ID uživatele!', 'danger');
			$this->redirect(':Admin:Users:');
		}
		$newPassword = $user->recoverPassword();
		$this->em->flush();
		$this->flashMessage("Uživateli $user->username bylo úspěšně obnovena heslo[$newPassword].", 'success');
		$this->redirect(':Admin:Users:');

	}

	public function handleChangeState($userId) {
		$user = $this->userRepository->findById($userId);
		if (!$user) {
			$this->flashMessage('Neplatné ID uživatele!', 'danger');
			$this->redirect(':Admin:Users:');
		}

		$action = ['deaktivován', 'aktivován'];
		$ac = $user->changeState();
		$this->em->flush();
		$this->flashMessage("Uživatel $user->username byl úspěšně $action[$ac]!", 'success');
		$this->redirect('this');
	}

	public function createComponentAddUserForm() {
		return $this->addUserFormFactory->create();
	}

	public function createComponentChangeRoleForm() {
		return $this->changeRoleFormFactory->create();
	}


}