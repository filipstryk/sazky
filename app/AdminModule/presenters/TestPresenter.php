<?php

namespace App\AdminModule\Presenters;

use Nette\Application\UI\Form;
use Nette\Utils\DateTime;

class TestPresenter extends BaseAdminPresenter
{
	public function createComponentTestForm() {
		$form = new Form();

		$form->addText('date', 'Datum:')
			->setDefaultValue((new DateTime('now'))->format("d.m.Y"));

		$form->addSubmit('send', 'Odeslat');

		$form->onSuccess[] = $this->testFormSuccess;

		return $form;
	}

	public function testFormSuccess(Form $form, $values) {
		\Tracy\Debugger::barDump(DateTime::from($values->date, 'Čas a datum:'));
	}
}