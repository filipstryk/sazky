<?php

namespace App\AdminModule\Components;

use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Kdyby\Doctrine\EntityManager;
use App\Model\Repositories\UserRepository;
use App\Model\Entities\UserEntity;

/**
 * Třda ChangeRoleForm
 * @package App\FrontModule\Components
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class ChangeRoleForm extends Control
{
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * @param UserRepository $userRepository
	 * @param EntityManager $entityManager
	 */
	public function __construct(UserRepository $userRepository, EntityManager $entityManager) {
		parent::__construct();
		$this->userRepository = $userRepository;
		$this->em = $entityManager;
	}

	/**
	 * @return Form
	 */
	public function createComponentChangeRoleForm() {
		$form = new Form();

		$form->addSelect('username', 'Uživatel:', $this->userRepository->getPairs());

		$form->addSelect('role', 'Role:', UserEntity::getRoleList());

		$form->addSubmit('send', 'Změnit roli');

		$form->onSuccess[] = $this->changeRoleFormSuccess;

		return $form;
	}

	/**
	 * @param Form $form
	 * @param array $values
	 */
	public function changeRoleFormSuccess(Form $form, $values) {
		if (!$this->presenter->user->isInRole('admin')) {
			$this->presenter->flashMessage('Nedostatečná oprávnění.', 'danger');
			$this->presenter->redirect(':Homepage:');
		}
		
		$user = $this->userRepository->findById($values->username);
		if (!$user) {
			$this->presenter->flashMessage("Neplatné ID uživatele!", 'danger');
			$this->presenter->redirect('this');
		}

		$user->setRole($values->role);
		$this->em->flush();
		$this->presenter->flashMessage("Uživateli $user->username byla úspěšně změněna role[$user->role]!", 'success');
		$this->presenter->redirect('this');
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/changeRoleForm.latte');
		$this->template->render();
	}
}

interface IChangeRoleFormFactory {
	/** @return ChangeRoleForm */
	function create();
}