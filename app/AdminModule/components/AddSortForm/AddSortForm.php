<?php

namespace App\AdminModule\Components;

use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Kdyby\Doctrine\EntityManager;
use App\Model\Entities\SortEntity;
use App\Model\Repositories\SortRepository;

/**
 * Třída AddSortForm
 * @package App\AdminModule\Components
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class AddSortForm extends Control
{
	/**
	 * @var SortRepository $sortRepository
	 */
	private $sortRepository;

	/**
	 * @var EntityManager $em
	 */
	private $em;

	/**
	 * @param EntityManager $entityManager
	 * @param SortRepository $sortRepository
	 */
	public function __construct(EntityManager $entityManager, SortRepository $sortRepository) {
		parent::__construct();
		$this->sortRepository = $sortRepository;
		$this->em = $entityManager;
	}

	/**
	 * @return Form
	 */
	public function createComponentAddSortForm() {
		$form = new Form();

		$form->addText('name', 'Název:')
			->addRule(Form::FILLED, 'Musíte zadat název druhu!');

		$form->addText('color', 'Barva:')
			->setType('color');

		$form->addSubmit('send', 'Přidat druh');

		$form->onSuccess[] = $this->addSortFormSuccess;

		return $form;
	}

	/**
	 * @param Form $form
	 * @param array $values
	 */
	public function addSortFormSuccess(Form $form, $values) {
		if (!$this->presenter->user->isInRole('admin')) {
			$this->presenter->flashMessage('Nedostatečná oprávnění.', 'danger');
			$this->presenter->redirect(':Homepage:');
		}

		$sort = $this->sortRepository->findByName($values->name);

		if($sort) {
			$this->presenter->flashMessage("Druh s názvem $values->name uý existuje!", 'danger');
			$this->presenter->redirect('this');
		}

		$sort = new SortEntity($values->name, $values->color);
		$this->em->persist($sort);
		$this->em->flush();
		$this->presenter->flashMessage("Druh $values->name byl úspěšně vytvořen!", 'success');
		$this->presenter->redirect('this');
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/addSortForm.latte');
		$this->template->render();
	}
}

interface IAddSortFormFactory {
	/** @return AddSortForm */
	function create();
}