<?php

namespace App\AdminModule\Components;

use Nette\Http\Session;
use Nette\Application\UI\Form;
use Nette\Http\SessionSection;
use Nette\Application\UI\Control;
use App\Model\Repositories\TypeRepository;

/**
 * Třída SelectTypeForm
 * @package App\AdminModule\Components
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class SelectTypeForm extends Control
{
	/**
	 * @var TypeRepository
	 */
	private $typeRepository;

	/**
	 * @var SessionSection
	 */
	private $sessionSection;

	public function __construct(TypeRepository $typeRepository, Session $session) {
		parent::__construct();
		$this->typeRepository = $typeRepository;
		$this->sessionSection = $session->getSection('addMatch');
	}

	/**
	 * @return Form
	 */
	public function createComponentSelectTypeForm() {
		$form = new Form();

		$form->addSelect('type', 'Typ:', $this->typeRepository->getPairs());

		$form->addSubmit('send', 'Vybrat');

		$form->onSuccess[] = $this->selectTypeFormSuccess;

		return $form;
	}

	public function selectTypeFormSuccess(Form $form, $values) {
		if (!$this->presenter->user->isInRole('admin')) {
			$this->presenter->flashMessage('Nedostatečná oprávnění.', 'danger');
			$this->presenter->redirect(':Homepage:');
		}

		$type = $this->typeRepository->findById($values->type);
		if (!$type) {
			$this->presenter->flashMessage('Neplatné ID typu!', 'danger');
			$this->presenter->redirect('this');
		}

		$this->sessionSection->selectedType = $type;
		$this->presenter->redirect('setMatchDetails');


	}

	public function render() {
		$this->template->setFile(__DIR__ . '/selectTypeForm.latte');
		$this->template->render();
	}
}

interface ISelectTypeFormFactory {
	/** @return SelectTypeForm */
	function create();
}