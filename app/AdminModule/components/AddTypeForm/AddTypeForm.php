<?php

namespace App\AdminModule\Components;

use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Kdyby\Doctrine\EntityManager;
use App\Model\Entities\TypeEntity;
use App\Model\Repositories\TypeRepository;
use App\Model\Repositories\SortRepository;

/**
 * Třída AddTypeForm
 * @package App\AdminModule\Components
 * @author Filip Stryl <filipstryk@hotmail.com>
 */
class AddTypeForm extends Control
{
	/**
	 * @var TypeRepository
	 */
	private $typeRepository;

	/**
	 * @var SortRepository
	 */
	private $sortRepository;


	/**
	 * @var EntityManager $em
	 */
	private $em;

	/**
	 * @param TypeRepository $typeRepository
	 * @param EntityManager $entityManager
	 */
	public function __construct(TypeRepository $typeRepository, SortRepository $sortRepository, EntityManager $entityManager) {
		parent::__construct();
		$this->typeRepository = $typeRepository;
		$this->sortRepository = $sortRepository;
		$this->em = $entityManager;
	}

	/**
	 * @return Form
	 */
	public function createComponentAddTypeForm() {
		$form = new Form();

		$form->addText('name', 'Název:')
			->addRule(Form::FILLED, 'Musíte zadat název typu!');

		$form->addText('color', 'Barva:')
			->setType('color');

		$form->addSelect('sort', 'Druh:', $this->sortRepository->getPairs());

		$form->addSubmit('send', 'Přidat typ');

		$form->onSuccess[] = $this->addTypeFormSuccess;

		return $form;
	}

	/**
	 * @param Form $form
	 * @param array $values
	 */
	public function addTypeFormSuccess(Form $form, $values) {
		if (!$this->presenter->user->isInRole('admin')) {
			$this->presenter->flashMessage('Nedostatečná oprávnění.', 'danger');
			$this->presenter->redirect(':Homepage:');
		}

		$type = $this->typeRepository->findByName($values->name);

		if($type) {
			$this->presenter->flashMessage("Typ s názvem $values->name už existuje!", 'danger');
			$this->presenter->redirect('this');
		}

		$sort = $this->sortRepository->findById($values->sort);
		$type = new TypeEntity($values->name, $values->color, $sort);
		$this->em->persist($type);
		$this->em->flush();
		$this->presenter->flashMessage("Typ $values->name byl úspěšně přidán!", 'success');
		$this->presenter->redirect('this');
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/addTypeForm.latte');
		$this->template->render();
	}
}

interface IAddTypeFormFactory {
	/** @return AddTypeForm */
	function create();
}