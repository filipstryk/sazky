<?php

namespace App\AdminModule\Components;

use Nette\Application\UI\Form;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use App\Model\Entities\TeamEntity;
use App\Model\Repositories\TypeRepository;
use App\Model\Repositories\TeamRepository;

/**
 * Třída AddTeamForm
 * @package App\AdminModule\Components
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class AddTeamForm extends Control
{
	/**
	 * @var TypeRepository $typeRepository
	 */
	private $typeRepository;

	/**
	 * @var TeamRepository $teamRepository
	 */
	private $teamRepository;

	/**
	 * @var EntityManager $em
	 */
	private $em;

	/**
	 * @param TypeRepository $typeRepository
	 * @param EntityManager $entityManager
	 */
	public function __construct(TypeRepository $typeRepository, TeamRepository $teamRepository, EntityManager $entityManager) {
		parent::__construct();
		$this->typeRepository = $typeRepository;
		$this->teamRepository = $teamRepository;
		$this->em = $entityManager;
	}

	/**
	 * @return Form
	 */
	public function createComponentAddTeamForm() {
		$form = new Form();

		$form->addText('name', 'Název:')
			->addRule(Form::FILLED, 'Musíze zadat název týmu!');

		$form->addSelect('type', 'Typ:', $this->typeRepository->getPairs());

		$form->addSubmit('send', 'Přidat tým');

		$form->onSuccess[] = $this->addTeamFormSuccess;

		return $form;
	}

	/**
	 * @param Form $form
	 * @param array $values
	 */
	public function addTeamFormSuccess(Form $form, $values) {
		if (!$this->presenter->user->isInRole('admin')) {
			$this->presenter->flashMessage('Nedostatečná oprávnění.', 'danger');
			$this->presenter->redirect(':Homepage:');
		}

		$type = $this->typeRepository->findById($values->type);

		$team = new TeamEntity($values->name, $type);
		$this->em->persist($team);
		$this->em->flush();
		$this->presenter->flashMessage("Tým $values->name byl úspěšně přidán!", 'success');
		$this->presenter->redirect('this');
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/addTeamForm.latte');
		$this->template->render();
	}
}

interface IAddTeamFormFactory {
	/** @return AddTeamForm */
	function create();
}