<?php

namespace App\AdminModule\Components;

use Nette\Utils\Random;
use Nette\Security\Password;
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;
use Kdyby\Doctrine\EntityManager;
use App\Model\Entities\UserEntity;
use App\Model\Repositories\UserRepository;

/**
 * Třída AddUserForm
 * @package App\FrontModule\Components
 * @author Filip Stryk <filipstryk@hotmail.com>
 */
class AddUserForm extends Control
{
	/**
	 * @var UserRepository $userRepository
	 */
	private $userRepository;

	/**
	 * @var EntityManager $em
	 */
	private $em;

	/**
	 * @param EntityManager $entityManager
	 */
	public function __construct(EntityManager $entityManager, UserRepository $userRepository) {
		parent::__construct();
		$this->em = $entityManager;
		$this->userRepository = $userRepository;
	}

	/**
	 * @return Form
	 */
	public function createComponentAddUserForm() {
		$form = new Form();

		$form->addText('username', 'Uživatelské jméno:')
			->addRule(Form::FILLED, 'Musíte zadat uživatelské jméno!');

		$form->addSelect('role', 'Role uživatele', UserEntity::getRoleList())
			->setDefaultValue('user');

		$form->addSubmit('send', 'Vytvořit uživatele');

		$form->onSuccess[] = $this->addUserFormSuccess;

		return $form;
	}

	/**
	 * @param Form $form
	 * @param array $values
	 */
	public function addUserFormSuccess(Form $form, $values) {
		if (!$this->presenter->user->isInRole('admin')) {
			$this->presenter->flashMessage('Nedostatečná oprávnění.', 'danger');
			$this->presenter->redirect(':Homepage:');
		}

		$user = $this->userRepository->findByUsername($values->username);

		if($user) {
			$this->presenter->flashMessage("Uživatel s uživatelským jménem $values->username už existuje!", 'danger');
			$this->presenter->redirect('this');
		}

		$password = Random::generate(15, '0-9a-zA-Z');
		$user = new UserEntity($values->username, $password, $values->role);
		$this->em->persist($user);
		$this->em->flush();
		$this->presenter->flashMessage("Uživatel $values->username[$values->role] s heslem $password byl úspěšně vytvořen!.", 'success');
		$this->presenter->redirect('this');
	}

	public function render() {
		$this->template->setFile(__DIR__ .'/addUserForm.latte');
		$this->template->render();
	}
}

interface IAddUserFormFactory {
	/** @return AddUserForm */
	function create();
}