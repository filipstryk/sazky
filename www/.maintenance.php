<?php

header('HTTP/1.1 503 Service Unavailable');
header('Retry-After: 300'); // 5 minutes in seconds

?>
<!DOCTYPE html>
<meta charset="utf-8">
<meta name="robots" content="noindex">
<meta name="generator" content="Nette Framework">

<style>
	.centered {
  		position: fixed;
  		top: 50%;
  		left: 50%;
  		margin-top: -200px;
  		margin-left: -250px;
	}
</style>

<img src="under_construction.jpg" alt="" class="centered">

<?php

exit;
